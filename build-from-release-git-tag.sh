#!/usr/bin/env bash
PKG_NAME="questionary"
WORKING_DIR=${HOME}/packaging-deb-${PKG_NAME}
# apply dfsg based on debian policy since I modified the real upstream (see below)
UPSTREAM_VERSION="1.9.0+dfsg"
DEBIAN_PKG_VERSION_SUFFIX="1"
PKG_DEBMAKE_WORKING_DIR_NAME=${PKG_NAME}-${UPSTREAM_VERSION}
PKG_DEBMAKE_WORKING_DIR=${WORKING_DIR}/${PKG_DEBMAKE_WORKING_DIR_NAME}
# setup.py is not used by the upstream source.
# I added setup.py which will be needed by dh-python, so let me put those modified "upstream" in my repo for
# convenience.
UPSTREAM_GIT_REPO_URL=https://salsa.debian.org/tai271828/${PKG_NAME}-wip.git

set -x

mkdir -p "${WORKING_DIR}"

# directory format {PKG_NAME}-${UPSTREAM_VERSION} is a must for debmake
git clone --no-checkout -o upstream ${UPSTREAM_GIT_REPO_URL} "${PKG_DEBMAKE_WORKING_DIR}"

pushd "${PKG_DEBMAKE_WORKING_DIR}"

git checkout -b debian/sid ${UPSTREAM_VERSION}

# prepare release tarball for a python package
#
# (NOT NEEDED because we use dh_make --createorig)
# several solutions to create source distribution and archive
#
# by using poetry the "upstream file may change" issue will happen since it differs from upstream source of git branch
#poetry build
#mv dist/${PKG_DEBMAKE_WORKING_DIR_NAME}.tar.gz ../
#
# by using git archive directly
#git archive --format=tar.gz -o ../${PKG_DEBMAKE_WORKING_DIR_NAME}.tar.gz v${UPSTREAM_VERSION}
#
# by using dh_make
# you may use template to handle the customization as well:
dh_make --createorig --python --yes -t ${HOME}/toolbox/scripts/packaging-deb-questionary/debian

# TODO not sure why not executable
chmod +x debian/rules

# pybuild is necessary to build python package via setup.py
# pybuild is setuptools based. that said, it assumes setup.py exists https://wiki.debian.org/Python/LibraryStyleGuide

# have no doc prepared yet
# and it has README.Debian not found. weird. relative path incorrect?
#rm -f debian/python-commitizen-doc.docs debian/README.Debian debian/README.source

# conventional manipulation to debianize python source
#
# dh_make generates more complicated or "raw" debian/* files to edit... headache, use debmake instead
# oh no, the template content of "dh_make --createorig" seems more accurate. let's use dh_make

# I use gbp below rather than debmake to build the package
#
## explicitly tell debmake that we want to generate python3 binary
#debmake -b:"python3"

realpath ./debian
git add ./debian
git commit -m "debianize the upstream source"

# s of 'v%(version)s' stands for string?
gbp buildpackage --git-ignore-new --git-builder=sbuild \
    --git-pristine-tar --git-pristine-tar-commit \
    --git-upstream-tag='%(version)s' --git-debian-branch=debian/sid

popd
