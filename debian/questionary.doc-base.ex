Document: questionary
Title: Debian questionary Manual
Author: <insert document author here>
Abstract: This manual describes what questionary is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/questionary/questionary.sgml.gz

Format: postscript
Files: /usr/share/doc/questionary/questionary.ps.gz

Format: text
Files: /usr/share/doc/questionary/questionary.text.gz

Format: HTML
Index: /usr/share/doc/questionary/html/index.html
Files: /usr/share/doc/questionary/html/*.html
